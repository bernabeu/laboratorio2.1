import sqlite3
from flask import Flask, jsonify, request
DATABASE_NAME = "residencia.db"

app = Flask(__name__)

def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn

def create_tables():
    tables = [
        """CREATE TABLE IF NOT EXISTS messages (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                msg TEXT NOT NULL)
        """
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)

def save_msg(msg):
    db = get_db()
    cursor = db.cursor()
    statement = "INSERT INTO messages(msg) VALUES (?)"
    cursor.execute(statement, [msg])
    db.commit()
    return True

def get_msg():
    db = get_db()
    cursor = db.cursor()
    query = "SELECT id, msg FROM messages"
    cursor.execute(query)
    return cursor.fetchall()

@app.route("/msg", methods=["POST"])
def save_message():
    msg_details = request.get_json()
    msg = msg_details["msg"]
    result = save_msg(msg)
    return jsonify(result)

@app.route("/msg", methods=["GET"])
def get_message():
    msg = get_msg()
    return jsonify(msg)

if __name__=="__main__":
    create_tables()
    app.run(host='0.0.0.0', port=7000, debug=False)

