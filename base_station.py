import time
from threading import Thread
from residencia import *
import serial
import serial.tools.list_ports as list_ports

PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 10

app = Flask(__name__)

@app.route("/msg", methods=["POST"])
def save_message():
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)
    ser_micro.open()
    msg_details = request.get_json()
    msg = msg_details["msg"]
    message = msg.encode('UTF-8')
    ser_micro.write(message)
    ser_micro.close()
    return msg

def find_comport(pid, vid, baud):
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None

def microReceiver():
    print('looking for microbit')
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)
    if not ser_micro:
        print('microbit not found')
        return

    print('opening and monitoring microbit port')
    ser_micro.open()
    while True:
        time.sleep(0.1)
        line = ser_micro.readline().decode('UTF-8')
        if line:
            print(line)
            save_msg(line)




if __name__=="__main__":
    create_tables()
    t = Thread(target=microReceiver)
    t.start()
    app.run(host="0.0.0.0", port=6000)